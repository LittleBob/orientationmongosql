<?php
if (!isset($_SESSION["user"])) {
    header("Location: index.php");
}
$action = $_REQUEST['action'];

switch ($action) {
    case 'AfficherEpreuve':
        {
            $lesEpreuves = $pdo->AfficherEdition();
            $edition = [];

            include "vues/v_ListeCourse.php";
            break;
        }
    case 'ETOILE':
        {//
            include "vues/v_etoile.php";
            break;
        }
    case 'Description':
        {
            $edition = $pdo->AfficherEditionById($_REQUEST["id"]);
            $editionMongo = $mongo->getCommentaire($_REQUEST["id"]);
            $NbParticipant = $pdo->NbPariticipantEdition($_REQUEST["id"]);
            $NbCommentaire = count($editionMongo);
            $moyenne = $mongo->getMoyenne($_REQUEST["id"]);
            $TestPoster = $pdo->VerificationPosterAvis($_SESSION["user"], $_REQUEST["id"]);
            $PeutPoster = $mongo->VerificationAvisPoster($_SESSION["user"],$_REQUEST["id"]);
            foreach ($edition as $UneEdition) {
                include "vues/v_description.php";
            }
            break;
        }
    case 'Avis':
        {
            if (!isset($_POST["Modifier"]))
            {
               $idParticipant = $pdo->GetIdParticipantByEmail($_SESSION["user"]);
            include "vues/v_AjouterAvis.php"; 
            }
            else 
            {
                $Commentaire = $mongo->getCommentaireByEmail($_SESSION["user"],$_REQUEST["id"]);
                include("vues/v_ModifierAvis.php");
            }
            //
            break;
        }
    case 'ModifierAvis':
        {
            echo $_SESSION["user"]." ".$_REQUEST["idEdition"]." ".$_POST["Note"]." ".$_POST["Commentaire"];
            $mongo->updateCommentaire($_SESSION["user"],$_REQUEST["idEdition"],$_POST["Note"],$_POST["Commentaire"]);
            header("Location:index.php?uc=Gerer&action=Description&id=".$_REQUEST["idEdition"]);
            break;
        }
    case 'AjouterAvis':
        {
            if ($_POST["Note"] >= 0 && $_POST["Note"] <= 5 && $_POST["Note"] != "" && $_POST["Commentaire"] != "") {
                if($mongo->addCommentaire("1", $_SESSION["user"], $pdo->GetParticipantByEmail($_SESSION["user"])["nom"], $pdo->GetParticipantByEmail($_SESSION["user"])["prenom"], $_POST["Commentaire"], $_POST["Note"], $_REQUEST["idEdition"]))
                {
                    header("Location: index.php?uc=Gerer&action=Description&id=" . $_REQUEST["idEdition"]);
                }
                
            } else {
                include "vues/v_AjouterAvis.php";
                include "vues/v_erreurAvis.php";
            }

            break;
        }

}
