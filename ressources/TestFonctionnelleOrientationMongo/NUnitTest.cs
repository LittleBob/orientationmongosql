﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium;
using NUnit.Framework;
using System.Threading;

namespace TestFonctionnelleOrientationMongo
{
    class NUnitTest
    {
        IWebDriver driver;
        [SetUp]
        public void Init()
        {
            driver = new EdgeDriver();
        }
        [Test]
        public void AjoutCommentaire()
        {
            driver.Url = "172.19.0.17/OrientationMongo/src1/index.php";
            IWebElement element = driver.FindElement(By.Name("user"));
            element.SendKeys("toto@titi.fr");
            element = driver.FindElement(By.Name("mdp"));
            element.SendKeys("1234");
            element = driver.FindElement(By.Name("SUBMIT"));
            element.Submit();
            element = driver.FindElement(By.Id("1"));
            element.Click();
            //Assert.IsTrue(driver.FindElement(By.Name("SUBMIT")).Displayed,"ERREUR");
            try
            {
                    driver.FindElement(By.Name("SUBMIT")).Submit();
                    driver.FindElement(By.Name("Note")).SendKeys("3.3");
                    driver.FindElement(By.Name("Commentaire")).SendKeys("Commentaire 10");
                    driver.FindElement(By.Name("SUBMIT")).Submit();
                    Thread.Sleep(5000);
                    Assert.AreEqual("Commentaire 10", driver.FindElements(By.Id("IDCOMMENTAIRE")).Last().Text, driver.FindElements(By.Id("IDCOMMENTAIRE")).Count.ToString());
                
            }
            catch (Exception)
            {
                    driver.FindElement(By.Name("Modifier")).Submit();
                    driver.FindElement(By.Name("Note")).SendKeys("1");
                    driver.FindElement(By.Name("Commentaire")).SendKeys("Commentaire 11");
                    driver.FindElement(By.Name("SubmitModifier")).Submit();
                    Thread.Sleep(5000);
                    Assert.AreEqual("Commentaire 11", driver.FindElements(By.Id("IDCOMMENTAIRE")).Last().Text, driver.FindElements(By.Id("IDCOMMENTAIRE")).Count.ToString());
                
            }
            
            


        }
        [TearDown]
        public void EndTest()
        {
            driver.Close();
        }
    }
}
