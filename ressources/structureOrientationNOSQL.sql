-- --------------------------------------------------------
-- Hôte :                        172.19.0.17
-- Version du serveur:           10.1.26-MariaDB-0+deb9u1 - Debian 9.1
-- SE du serveur:                debian-linux-gnu
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Listage de la structure de la base pour OrientationMongo
CREATE DATABASE IF NOT EXISTS `OrientationMongo` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `OrientationMongo`;

-- Listage de la structure de la table OrientationMongo. club
CREATE TABLE IF NOT EXISTS `club` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `contact` varchar(128) NOT NULL,
  `adresse` varchar(128) NOT NULL,
  `cp` varchar(5) NOT NULL,
  `ville` varchar(128) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table OrientationMongo. edition
CREATE TABLE IF NOT EXISTS `edition` (
  `id` int(11) NOT NULL,
  `description` varchar(1024) NOT NULL,
  `id_epreuve` int(11) DEFAULT NULL,
  `annee` int(11) DEFAULT NULL,
  `dateEpreuve` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_edition_epreuve` (`id_epreuve`),
  CONSTRAINT `FK_edition_epreuve` FOREIGN KEY (`id_epreuve`) REFERENCES `epreuve` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table OrientationMongo. epreuve
CREATE TABLE IF NOT EXISTS `epreuve` (
  `id` int(11) NOT NULL,
  `libelle` varchar(128) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `club` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_epreuve_club` (`club`),
  CONSTRAINT `FK_epreuve_club` FOREIGN KEY (`club`) REFERENCES `club` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table OrientationMongo. equipe
CREATE TABLE IF NOT EXISTS `equipe` (
  `id` int(11) NOT NULL,
  `nomEquipe` varchar(50) DEFAULT NULL,
  `couleur` varchar(50) DEFAULT NULL,
  `mdp` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table OrientationMongo. participant
CREATE TABLE IF NOT EXISTS `participant` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `equipe` int(11) DEFAULT NULL,
  `mdp` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_participant_equipe` (`equipe`),
  CONSTRAINT `FK_participant_equipe` FOREIGN KEY (`equipe`) REFERENCES `equipe` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
-- Listage de la structure de la table OrientationMongo. participer
CREATE TABLE IF NOT EXISTS `participer` (
  `id_participant` int(11) NOT NULL,
  `id_edition` int(11) NOT NULL,
  PRIMARY KEY (`id_participant`,`id_edition`),
  KEY `FK_participer_edition` (`id_edition`),
  CONSTRAINT `FK_participer_edition` FOREIGN KEY (`id_edition`) REFERENCES `edition` (`id`),
  CONSTRAINT `FK_participer_participant` FOREIGN KEY (`id_participant`) REFERENCES `participant` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Les données exportées n'étaient pas sélectionnées.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
