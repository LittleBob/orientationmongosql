﻿<?php
/**
 * Classe d'accès aux données.

 * Utilise les services de la classe PDO
 * pour l'application lafleur
 * Les attributs sont tous statiques,
 * les 4 premiers pour la connexion
 * $monPdo de type PDO
 * $monPdoGsb qui contiendra l'unique instance de la classe
 *
 * @package default
 * @author Patrice Grand
 * @version    1.0
 * @link       http://www.php.net/manual/fr/book.pdo.php
 */

class PdoOrientation
{
    private static $serveur = 'mysql:host=172.19.0.17';
    private static $bdd = 'dbname=OrientationMongo';
    private static $user = 'sio';
    private static $mdp = '0550002D';
    private static $monPdo;
    private static $monPdoOrientation = null;
/**
 * Constructeur privé, crée l'instance de PDO qui sera sollicitée
 * pour toutes les méthodes de la classe
 */
    private function __construct()
    {
        PdoOrientation::$monPdo = new PDO(PdoOrientation::$serveur . ';' . PdoOrientation::$bdd, PdoOrientation::$user, PdoOrientation::$mdp);
        PdoOrientation::$monPdo->query("SET CHARACTER SET utf8");
    }
    public function _destruct()
    {
        PdoOrientation::$monPdo = null;
    }
/**
 * Fonction statique qui crée l'unique instance de la classe
 *
 * Appel : $instancePdolafleur = PdoLafleur::getPdoLafleur();
 * @return l'unique objet de la classe PdoLafleur
 */
    public static function getPdoOrientation()
    {
        if (PdoOrientation::$monPdoOrientation == null) {
            PdoOrientation::$monPdoOrientation = new PdoOrientation();
        }
        return PdoOrientation::$monPdoOrientation;
    }
/**
 * Retourne toutes les catégories sous forme d'un tableau associatif
 *
 * @return le tableau associatif des catégories
 */
    public function AfficherEdition()
    {
        //SELECT libelle, annee, nom FROM club INNER JOIN epreuve ON epreuve.club = club.id INNER JOIN edition ON edition.id_epreuve = epreuve.id ORDER BY annee
        $req = "SELECT longitude,latitude,edition.id,edition.id_epreuve,libelle, annee, nom, club.cp FROM club INNER JOIN epreuve ON epreuve.club = club.id INNER JOIN edition ON edition.id_epreuve = epreuve.id ORDER BY annee";
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
#SELECT longitude,latitude,ville,cp,adresse,contact,epreuve.description as DescriptionEpreuve ,edition.description as DescriptionEdition ,dateEpreuve,libelle, annee, nom FROM club INNER JOIN epreuve ON epreuve.club = club.id INNER JOIN edition ON edition.id_epreuve = epreuve.id where edition.id = 1
    public function NbPariticipantEdition($idEdition)
    {
        $req = "select count(id_participant) as NbParticipant from participer where id_edition = " . $idEdition;
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
    public function AfficherEditionById($idEdition)
    {
        $req = "SELECT longitude,latitude,ville,cp,adresse,contact,epreuve.description as DescriptionEpreuve ,edition.description as DescriptionEdition ,dateEpreuve,libelle, annee, nom FROM club INNER JOIN epreuve ON epreuve.club = club.id INNER JOIN edition ON edition.id_epreuve = epreuve.id where edition.id = " . $idEdition;
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
    public function GetLesEpreuves()
    {
        $req = "select * from epreuve";
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }

    public function getEditionByIdEpreuve($idEpreuve)
    {
        $req = "select * from edition where id_epreuve = "+$idEpreuve;
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
    public function getClubById($idClub)
    {
        $req = "select * from club where id = "+$idClub;
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
    public function GetLesEditions()
    {
        $req = "select * from edition";
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes;
    }
    public function VerificationConnexion($user, $mdp)
    {
        $req = "select * from participant where email = '" . $user . "'";
        $res = PdoOrientation::$monPdo->query($req);
        $resultat = false;
        while ($ligne = $res->fetch()) {
            if ($user == $ligne["email"] && $mdp == $ligne["mdp"]) {
                $resultat = true;
            } else {
                $resultat = false;
            }
        }
        return $resultat;
    }
    public function VerificationPosterAvis($idParticipant, $idEdition)
    {
        //$req = "select count(*) as Participer from participer where id_participant = (select id from participant where email = '" . $idParticipant . "') AND id_edition = " . $idEdition;
        $req = "select CheckParticiper(".$this->GetIdParticipantByEmail($idParticipant).",".$idEdition.") as `Participer`;";
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetch();
        $resultat = false;
        if ($lesLignes["Participer"] == 1) {
            $resultat = true;
        }
        return $resultat;
    }
    public function GetParticipantByEmail($email)
    {
        $req = 'select * from participant where email = "' . $email . '"';
        $res = PdoOrientation::$monPdo->query($req);
        $lesLignes = $res->fetchAll();
        return $lesLignes[0];
    }
    public function GetIdParticipantByEmail($email)
    {
        return $this->GetParticipantByEmail($email)["id"];
    }
}
