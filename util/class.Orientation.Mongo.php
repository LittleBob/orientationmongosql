<?php
class MongoOrientation
{
    private static $serveur = '172.19.0.7';
    private static $mng;
    private static $monMng = null;
/**
 * Constructeur privé, crée l'instance de Mongo qui sera sollicitée
 * pour toutes les méthodes de la classe
 */
    private function __construct()
    {
        MongoOrientation::$mng = new MongoDB\Driver\Manager("mongodb://" . MongoOrientation::$serveur . ":27017/Orientation");
    }
    public function _destruct()
    {
        MongoOrientation::$monMng = null;
    }
/**
 * Fonction statique qui crée l'unique instance de la classe
 */
    public static function getManagerMongo()
    {
        if (MongoOrientation::$monMng == null) {
            MongoOrientation::$monMng = new MongoOrientation();
        }
        return MongoOrientation::$monMng;
    }
    // Ajout d'un commentaire
    public function addCommentaire($id, $mail, $nom, $prenom, $commentaire, $note, $idEdition)
    {
        $test = true;
        $array = MongoOrientation::getCommentaire($idEdition);
        foreach ($array as $a) {
            if ($a->email == $mail) {
                $test = false;
            }
        }
        if ($test) {
            $bulk = new MongoDB\Driver\BulkWrite;
            $avis = ['_id' => new MongoDB\BSON\ObjectID, 'id' => $id, 'email' => $mail, 'nom' => $nom, 'prenom' => $prenom, 'commentaire' => $commentaire, 'note' => $note, 'idEdition' => intval($idEdition)];
            $bulk->insert($avis);
            MongoOrientation::$mng->executeBulkWrite('Orientation.commentaires', $bulk);
            return true;
        }
        return false;

    }
    public function updateCommentaire($email, $idEdition, $note, $commentaire)
    {
        $bulk = new MongoDB\Driver\BulkWrite;
        $bulk->update(
            ['email' => $email, 'idEdition' => intval($idEdition)],
            ['$set' => ['note' => $note, 'commentaire' => $commentaire]]
        );
        MongoOrientation::$mng->executeBulkWrite('Orientation.commentaires', $bulk);
    }
    public function VerificationAvisPoster($Email, $idEdition)
    {
        $array = MongoOrientation::getCommentaire($idEdition);
        if (count($array) > 0) {
            foreach ($array as $a) {
                if ($a->email == $Email) {
                    return true;
                }
            }
        }
        return false;
    }
    public function getCommentaireByEmail($Email, $idEdition)
    {
        $LesCommentaires = MongoOrientation::getCommentaire($idEdition);
        for ($i = 0; $i < count($LesCommentaires); $i++) {
            if ($Email == $LesCommentaires[$i]->email) {
                return $LesCommentaires[$i];
            }
        }
        return false;
    }
    //Obtention des commentaires selon l'id d'une course
    public function getCommentaire($idEdition)
    {
        $filter = ['idEdition' => intval($idEdition)];
        $query = new MongoDB\Driver\Query($filter);

        $res = MongoOrientation::$mng->executeQuery("Orientation.commentaires", $query);

        $commentaires = $res->toArray();

        if (!empty($commentaires)) {
            return $commentaires;
        } else {
            return null;
        }
    }
    //Obtention de la moyenne des notes selon l'id d'une course
    public function getMoyenne($idEdition)
    {
        $filter = ['idEdition' => intval($idEdition)];
        $query = new MongoDB\Driver\Query($filter);

        $commentaires = MongoOrientation::$mng->executeQuery("Orientation.commentaires", $query);
        $moyenne = 0;
        $nbCommentaires = 0;

        if (!$commentaires->isDead()) {
            foreach ($commentaires as $com) {
                $moyenne += $com->note;
                $nbCommentaires++;
            }
            return round($moyenne / $nbCommentaires, 2);
        } else {
            return null;
        }
    }
}
