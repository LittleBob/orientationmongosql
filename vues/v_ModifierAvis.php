<form role="form" form method='POST' action='index.php?uc=Gerer&action=ModifierAvis&idEdition=<?php echo $_REQUEST["id"]?>&idParticipant=<?php echo $idParticipant?>'>
    <div class="jumbotron">
        <div class="form-group">
            Modifier Votre avis
            <br><br>
            <label for="NoteInput">
                Note
            </label>
            <input type="number" placeholder="<?php echo $Commentaire->note; ?>" step="0.1" max="5" min="0" class="form-control" name="Note" id="NoteInput">
        </div>
        <div class="form-group">
            <label for="CommentaireInput">
                Commentaire
            </label>
            <input type="text" placeholder="<?php echo $Commentaire->commentaire; ?>" class="form-control" name="Commentaire" id="CommentaireInput">
        </div>
        <button type="submit" name="SubmitModifier" class="btn btn-primary">
            Modifier l'avis
        </button>
    </div>
</form>