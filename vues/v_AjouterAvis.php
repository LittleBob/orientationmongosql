<form role="form" form method='POST' action='index.php?uc=Gerer&action=AjouterAvis&idEdition=<?php echo $_REQUEST["id"]?>&idParticipant=<?php echo $idParticipant?>' >
    <div class="jumbotron">
        <div class="form-group">
            <label for="NoteInput">
                Note
            </label>
            <input type="number" placeholder="Note uniquement entre 0 et 5" step="0.1" max="5" min="0" class="form-control" name="Note" id="NoteInput">
        </div>
        <div class="form-group">
            <label for="CommentaireInput">
                Commentaire
            </label>
            <input type="text" placeholder="Commentaire..." class="form-control" name="Commentaire" id="CommentaireInput">
        </div>
        <button type="submit" name="SUBMIT" class="btn btn-primary">
            Ajouter un avis
        </button>
    </div>
</form>