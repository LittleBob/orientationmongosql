<!DOCTYPE html>
<html>
<head>
<!-- Font Awesome Icon Library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
<style>

/*---------- star rating ----------*/
.star-rating, .back-stars, .front-stars {
  display: flex;
}

.star-rating {
  align-items: center;
  font-size: 3em;
  justify-content: center;
  margin-top: 50px;
  height:50px;
  position:relative;
  width: 145px;
}

.back-stars {
  color: #bb5252;
  position: absolute;
  right:0;
  left:0;
  top:0;
  bottom:0;
  text-shadow: 4px 4px 10px #843a3a;
}

.front-stars {
  color: #FFBC0B;
  position: absolute;
  left:0;
  top:0;
  overflow:hidden;
  bottom:0;
  text-shadow: 2px 2px 5px #d29b09;
  top: 0;
}
</style>
</head>
<script src="https://use.fontawesome.com/f4e64b7c17.js"></script>
<div class="star-rating">
  <div class="back-stars">
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>
  </div>
  <div class="front-stars" style="width: 150%">
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>
    <i class="fa fa-star" aria-hidden="true"></i>

  </div>
</div>
</html>
