<div style="background-color:rgb(237,210,229); color:rgb(203,28,128); font-size:1.1em; width:95%;">
<?php
$erreur = "";
if ($_POST["Note"] < 0)
{
    $erreur = $erreur."La note doit être superieur a 5<br>";
}
if ($_POST["Note"] > 5)
{
    $erreur = $erreur."La note doit être inferieur a 5<br>";
}
if ($_POST["Note"] == "")
{
    $erreur = $erreur."La note est vide<br>";
}
if ($_POST["Commentaire"] == "")
{
    $erreur = $erreur."Le commentaire est vide<br>";
}
if (!is_float($_POST["Note"]))
{
    $erreur = $erreur."La note n'est pas un nombre<br>";
}
echo $erreur;
?>
</div>