<body>
    <script>
    var lesCourses = JSON.parse('<?php echo json_encode($lesEpreuves);?>');
    </script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
    <div id="containerMap" style="height : 400px;width : 100%; z-index: 1; position: relative;">
        <div id="map" style="height : 400px;width : 100%;">
            <!-- Ici s'affichera la carte -->
        </div>
    </div>
    <div class="container" style="box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important; margin-top: -80px; z-index: 1; background: white;border-radius: 5px; position: relative;">
        </br>
        <div class="row justify-content-center">
            <div class="row justify-content-center">
                <input type="text" class="form-control" id="searchInput" placeholder="Enter un code postal" style="margin-left : 1.5%;">
                <!-- <button type="button" class="btn btn-success"> Rechercher </button>-->
            </div>
        </div></br>
        <div class="container">
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>
                            Numero de l'edition
                        </th>
                        <th>
                            Nom de l'epreuve
                        </th>
                        <th>
                            Date
                        </th>
                        <th>
                            Nom du club
                        </th>
                        <th>
                        Code postal
                        </th>
                    </tr>
                </thead>
                <tbody id="tableBody">
                    <?php
                        foreach ($lesEpreuves as $Epreuve) {
							if (isset($edition[$Epreuve["id_epreuve"]])) {
								$edition[$Epreuve["id_epreuve"]] += 1;
							} else {
								$edition[$Epreuve["id_epreuve"]] = 1;
							}
                            echo "<tr id=".$Epreuve["id"].">
							<th>
							".$edition[$Epreuve["id_epreuve"]]."
							</th>
							<th>
								".$Epreuve["libelle"]."
                            </th>
                            <th>
                            ".$Epreuve["annee"]."
							</th>
							<th>
							".$Epreuve["nom"]."
                            </th>
                            <th>
                            ".$Epreuve["cp"]."
                            </th>
						</tr>";
                        }#AFFICHER LE NOM DE L'EPREUVE, AFFICHER LA DATE DE L'EDITION LE NUMERO DE L'EDITION ET LE NOM DU CLUB 
                        ?>
                </tbody>
            </table>
        </div></br>
    </div>
</body>