<br />
<div style="text-align:center;">
    <h2>
        Courses d'orientation national
    </h2>
    <p>
        La plate forme pour s'informer et évaluer les courses d'orientation ayant lieu en France.
    </p>
</div>
<br />
<form role="form" form method='POST' action='index.php?uc=Connexion&action=Connexion'>
    <div class="jumbotron " style="width:30%;margin: auto;">
        <?php if(isset($error)){
            echo '<div id="ErreurConnexion" class="alert alert-danger" role="alert">
            Email ou mot de passe incorrect !
        </div>';}?>
        <div class="form-group">
            <label for="emailInput">
                Email
            </label>
            <input type="email" class="form-control" name="user" id="emailInput">
        </div>
        <div class="form-group">
            <label for="passwordInput">
                Mot de passe
            </label>
            <input type="password" class="form-control" name="mdp" id="passwordInput">
        </div>
        <button type="submit" name="SUBMIT" class="btn btn-primary">
            Connexion
        </button>
    </div>
</form>