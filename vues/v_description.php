<script>
var lesCourses = JSON.parse('<?php echo json_encode($UneEdition);?>');
</script>

<body>
    <div id="containerMap" style="height : 400px;width : 100%; z-index: 1; position: relative;">
        <div id="map" style="height : 400px;width : 100%;">
            <!-- Ici s'affichera la carte -->
        </div>
    </div>
    <div class="container" style="box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important; margin-top: -80px; z-index: 1; background: white;border-radius: 5px; position: relative;">
        <div class="container">
            <p style="text-align: center;"><strong><?php echo $UneEdition["libelle"]." ".$UneEdition["annee"]; ?> </strong></p>
            <p>Déroulant le <?php echo date('d/m/Y', strtotime($UneEdition["dateEpreuve"]))." à ".$UneEdition["adresse"]; ?></p>
            <strong>
                <p>Description de l'édition:</p>
            </strong>
            <p><?php echo $UneEdition["DescriptionEdition"];?> </p>
            <strong>
                <p>Description de l'épreuve : </p>
            </strong>
            <p><?php echo $UneEdition["DescriptionEpreuve"];?> </p>
            <address>
                <strong>Coordonnées et contact</strong><br> <?php echo $UneEdition["contact"] ?></br>
                <strong>Coordonnées GPS : </strong><br> <?php echo $UneEdition["latitude"].",".$UneEdition["longitude"] ?>
            </address>
        </div>
        <div class="container avis text-center">
            <h5>
                <div>
                    <?php echo $NbParticipant[0]["NbParticipant"]; ?> Inscrits en <?php echo $UneEdition["annee"];?> - <?php echo $NbCommentaire ?> l'ont évalué
                    <?php if ($NbCommentaire >= 1) { echo "Evaluée à ".$moyenne." sur 5 ";}?> </div>
            </h5>
            <?php
            if ($TestPoster && !$PeutPoster)
            {
                echo "
                <form role='form' form method='POST' action='index.php?uc=Gerer&action=Avis&id=".$_REQUEST['id']."' >
                <button type='submit' name='SUBMIT' class='btn btn-primary'>Ajouter un avis</button>
                </form>
                ";
            }
            else if ($TestPoster && $PeutPoster)
            {
                echo "
                <form role='form' form method='POST' action='index.php?uc=Gerer&action=Avis&id=".$_REQUEST['id']."' >
                <button type='submit' name='Modifier' class='btn btn-primary'>Modifier un avis</button>
                </form>
                ";
            }
            ?>

            <div id="test" class="container "></br>
                <?php 
                    if ($NbCommentaire >= 1)
                    {
                        for ($i=0; $i < $NbCommentaire ; $i++) {
                            include("vues/v_commentaire.php");
                        }
                    }
                    else
                    {
                        include("vues/v_aucun_commentaire.php");
                    }
                ?>
            </div>
            </br>
        </div>
    </div>


    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
</body>