$(() => {
    //Fonction permettant de cliquer sur les lignes de la liste des épreuves
    function trClick() {
        $("tr").click(function () {
            idEdition = $(this).attr("id");
            if (idEdition != undefined)
                window.location = "index.php?uc=Gerer&action=Description&id=" + idEdition;
        });
    }
    // On initialise la latitude et la longitude de Paris (centre de la carte)
    var lat = 48.852969;
    var lon = 2.349903;
    var macarte = null;
    var markers = new Array();
    // Fonction d'initialisation de la carte
    function initMap() {
        // Créer l'objet "macarte" et l'insèrer dans l'élément HTML qui a l'ID "map"
        macarte = L.map('map').setView([lesCourses.length > 0 ? lesCourses[0].latitude : lesCourses.latitude, lesCourses.length > 0 ? lesCourses[0].longitude : lesCourses.longitude], lesCourses.length > 1 ? 8 : 11);
        // Leaflet ne récupère pas les cartes (tiles) sur un serveur par défaut. Nous devons lui préciser où nous souhaitons les récupérer. Ici, openstreetmap.fr
        L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
            // Il est toujours bien de laisser le lien vers la source des données
            attribution: 'données © <a href="//osm.org/copyright">OpenStreetMap</a>/ODbL - rendu <a href="//openstreetmap.fr">OSM France</a>',
            minZoom: 1,
            maxZoom: 20
        }).addTo(macarte);
        if (lesCourses.length > 0) {
            lesCourses.forEach(e => {
                var marker = L.marker([e.latitude, e.longitude]);
                marker.bindPopup(e.nom + "</br>" + e.libelle);
                markers.push(marker);
                macarte.addLayer(marker);

            });
        } else {
            var marker = L.marker([lesCourses.latitude, lesCourses.longitude]);
            marker.bindPopup(lesCourses.nom + "</br>" + lesCourses.libelle);
            markers.push(marker);
            macarte.addLayer(marker);
        }

    }
    initMap();
    trClick();
    //Actualise les marqueurs de la carte
    function reloadMap(array) {
        for (i = 0; i < markers.length; i++) {
            macarte.removeLayer(markers[i]);
        }
        if (array.length != 0) {
            if (array.length > 0) {
                array.forEach(e => {
                    var marker = L.marker([e.latitude, e.longitude]);
                    marker.bindPopup(e.nom + "</br>" + e.libelle);
                    markers.push(marker);
                    macarte.addLayer(marker);
                });
            } else {
                var marker = L.marker([array.latitude, array.longitude]);
                marker.bindPopup(array.nom + "</br>" + array.libelle);
                markers.push(marker);
                macarte.addLayer(marker);
            }
            if (array.length != lesCourses.length) {
                macarte.setView([array.length > 0 ? array[0].latitude : array.latitude, array.length > 0 ? array[0].longitude : array.longitude], 11);
            } else {
                macarte.setView([array.length > 0 ? array[0].latitude : array.latitude, array.length > 0 ? array[0].longitude : array.longitude], 8);
            }
        } else {
            macarte.setView([50, 2], 8);
        }
    }

    //Search Input
    $("#searchInput").on("input propertychange", () => {
        var value = $("#searchInput").val();
        var searchTable = 'Aucun résultat';
        var points = [];
        var edition = [];
        if (value != "") {
            if (lesCourses.length > 0) {
                lesCourses.forEach(e => {
                    if (e.cp.indexOf(value) != -1) {
                        if (edition[e.id_epreuve] != undefined) {
                            edition[e.id_epreuve] += 1;
                        } else {
                            edition[e.id_epreuve] = 1;
                        }
                        searchTable += '<tr id="' + e.id + '"><th>' + edition[e.id_epreuve] + '</th><th>' + e.libelle + '</th><th>' + e.annee + '</th><th>' + e.nom + '</th><th>' + e.cp + '</th></tr>';
                        points.push(e);
                    }
                });
            } else {
                if (e.cp.indexOf(value) != -1) {
                    if (edition[e.id_epreuve] != undefined) {
                        edition[e.id_epreuve] += 1;
                    } else {
                        edition[e.id_epreuve] = 1;
                    }
                    searchTable += '<tr id="' + e.id + '"><th>' + edition[e.id_epreuve] + '</th><th>' + e.libelle + '</th><th>' + e.annee + '</th><th>' + e.nom + '</th><th>' + e.cp + '</th></tr>';
                    points.push(e);
                }
            }
        } else {
            lesCourses.forEach(e => {
                if (edition[e.id_epreuve] != undefined) {
                    edition[e.id_epreuve] += 1;
                } else {
                    edition[e.id_epreuve] = 1;
                }
                searchTable += '<tr id="' + e.id + '"><th>' + edition[e.id_epreuve] + '</th><th>' + e.libelle + '</th><th>' + e.annee + '</th><th>' + e.nom + '</th><th>' + e.cp + '</th></tr>';
                points.push(e);
            });
        }
        $("#tableBody").html(searchTable);
        trClick();
        reloadMap(points);
    });
});